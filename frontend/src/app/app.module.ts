import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { TodoComponent } from './components/todo/todo.component'
import { TodoListComponent } from './components/todo-list/todo-list.component'
import { TodoInputComponent } from './components/todo-input/todo-input.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [AppComponent, TodoComponent, TodoListComponent, TodoInputComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
