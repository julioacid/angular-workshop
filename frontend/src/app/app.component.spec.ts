import { FormsModule } from '@angular/forms'
import { getByAltText, getByText, queryByText, render, screen } from '@testing-library/angular'
import { TodoMemoryService } from '../test/services/todo-memory.service'
import { AppComponent } from './app.component'
import { TodoInputComponent } from './components/todo-input/todo-input.component'
import { TodoListComponent } from './components/todo-list/todo-list.component'
import { TodoComponent } from './components/todo/todo.component'
import { TodoService } from './services/todo.service'
import userEvent from '@testing-library/user-event'

describe.only('AppComponent', () => {
  beforeEach(async () => {
    await render(AppComponent, {
      declarations: [TodoListComponent, TodoInputComponent, TodoComponent],
      imports: [FormsModule],
      providers: [{ provide: TodoService, useClass: TodoMemoryService }],
    })
  })

  const getTodoByTitle = (title: string) => <HTMLElement>screen.getByText(title).closest('app-todo')

  it('adds a new todo', async () => {
    userEvent.type(screen.getByPlaceholderText('Title'), 'My title')
    userEvent.type(screen.getByPlaceholderText('Description'), 'My description')
    userEvent.click(screen.getByText('Submit'))

    await expect(screen.findByText('My title')).resolves.toBeInTheDocument()
    await expect(screen.findByText('My description')).resolves.toBeInTheDocument()
  })

  it('completes a todo', async () => {
    userEvent.type(screen.getByPlaceholderText('Title'), 'My title')
    userEvent.type(screen.getByPlaceholderText('Description'), 'My description')
    userEvent.click(screen.getByText('Submit'))
    const todoBeforeAction = getTodoByTitle('My title')

    userEvent.click(getByText(todoBeforeAction, 'Complete'))
    const todoAfterAction = getTodoByTitle('My title')

    expect(getByText(todoAfterAction, 'TODO completed')).toBeInTheDocument()
    expect(queryByText(todoAfterAction, 'Complete')).not.toBeInTheDocument()
  })

  it('deletes a todo', async () => {
    userEvent.type(screen.getByPlaceholderText('Title'), 'My title')
    userEvent.type(screen.getByPlaceholderText('Description'), 'My description')
    userEvent.click(screen.getByText('Submit'))
    const todoBeforeAction = getTodoByTitle('My title')

    userEvent.click(getByAltText(todoBeforeAction, 'Delete button'))

    expect(screen.queryByText('My title')).not.toBeInTheDocument()
    expect(screen.queryByText('My description')).not.toBeInTheDocument()
  })
})
