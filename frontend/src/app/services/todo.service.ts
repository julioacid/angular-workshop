import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { switchMap, tap } from 'rxjs/operators'
import { Todo } from '../models/todo'
import { ApiService } from './api.service'

@Injectable({ providedIn: 'root' })
export class TodoService {
  private fetchTodos$$ = new BehaviorSubject<void>(undefined)

  constructor(private api: ApiService) {}

  getTodos() {
    return this.fetchTodos$$.pipe(switchMap(() => this.api.get<Todo[]>('todos')))
  }

  addTodo({ title, description }: Pick<Todo, 'title' | 'description'>) {
    return this.api.post<Todo>('todos', { title, description }).pipe(this.fetchTodos())
  }

  completeTodo(id: string) {
    return this.api.patch<Todo>(`todos/${id}/complete`).pipe(this.fetchTodos())
  }

  deleteTodo(id: string) {
    return this.api.delete<void>(`todos/${id}`).pipe(this.fetchTodos())
  }

  private fetchTodos() {
    return (obs: Observable<any>) => obs.pipe(tap(() => this.fetchTodos$$.next()))
  }
}
