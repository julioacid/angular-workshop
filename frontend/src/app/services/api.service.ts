import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable({ providedIn: 'root' })
export class ApiService {
  private readonly BASE_URL = 'http://localhost:8080'

  constructor(private http: HttpClient) {}

  get<T>(url: string) {
    return this.http.get<T>(this.buildUrl(url))
  }

  post<T>(url: string, body?: any) {
    return this.http.post<T>(this.buildUrl(url), body)
  }

  delete<T>(url: string) {
    return this.http.delete<T>(this.buildUrl(url))
  }

  patch<T>(url: string, body?: any) {
    return this.http.patch<T>(this.buildUrl(url), body)
  }

  private buildUrl(url: string) {
    return `${this.BASE_URL}/${url}`
  }
}
