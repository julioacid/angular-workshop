import { render, screen } from '@testing-library/angular'
import { COMPLETED_TODO, PENDING_TODO } from '../../../test/fixtures'
import { TodoComponent } from './todo.component'

describe('TodoComponent', () => {
  it('renders a pending todo', async () => {
    await render(TodoComponent, { componentProperties: { todo: PENDING_TODO } })

    expect(screen.getByText(PENDING_TODO.id)).toBeInTheDocument()
    expect(screen.getByAltText('Delete button')).toBeInTheDocument()
    expect(screen.getByText(PENDING_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(PENDING_TODO.description)).toBeInTheDocument()
    expect(screen.getByText('Complete')).toBeInTheDocument()
    expect(screen.queryByText('TODO completed')).not.toBeInTheDocument()
  })

  it('renders a completed todo', async () => {
    await render(TodoComponent, { componentProperties: { todo: COMPLETED_TODO } })

    expect(screen.getByText(COMPLETED_TODO.id)).toBeInTheDocument()
    expect(screen.getByAltText('Delete button')).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO.description)).toBeInTheDocument()
    expect(screen.queryByText('Complete')).not.toBeInTheDocument()
    expect(screen.getByText('TODO completed')).toBeInTheDocument()
  })
})
