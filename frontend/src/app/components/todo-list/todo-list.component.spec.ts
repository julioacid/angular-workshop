import { render, screen } from '@testing-library/angular'
import { of } from 'rxjs'
import { TODOS } from '../../../test/fixtures'
import { TodoService } from '../../services/todo.service'
import { TodoComponent } from '../todo/todo.component'
import { TodoListComponent } from './todo-list.component'

describe('TodoListComponent', () => {
  it('renders', async () => {
    await render(TodoListComponent, {
      declarations: [TodoComponent],
      providers: [
        {
          provide: TodoService,
          useValue: { getTodos: () => of(TODOS) },
        },
      ],
    })

    TODOS.forEach(
      async (todo) => await expect(screen.findByText(todo.id)).resolves.toBeInTheDocument()
    )
  })
})
