import { Component } from '@angular/core'
import { Observable, Subscription } from 'rxjs'
import { Todo } from '../../models/todo'
import { TodoService } from '../../services/todo.service'

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent {
  todos$!: Observable<Todo[]>

  subs = new Subscription()

  constructor(private todoService: TodoService) {
    this.todos$ = this.todoService.getTodos()
  }

  completeTodo({ id }: Todo) {
    const subscription = this.todoService.completeTodo(id).subscribe()

    this.subs.add(subscription)
  }

  deleteTodo({ id }: Todo) {
    const subscription = this.todoService.deleteTodo(id).subscribe()

    this.subs.add(subscription)
  }
}
