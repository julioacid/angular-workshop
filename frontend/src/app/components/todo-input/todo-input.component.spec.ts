import { FormsModule } from '@angular/forms'
import { render, screen } from '@testing-library/angular'
import { TodoService } from '../../services/todo.service'
import { TodoInputComponent } from './todo-input.component'
import userEvent from '@testing-library/user-event'

describe('TodoInputComponent', () => {
  it('renders', async () => {
    await render(TodoInputComponent, {
      providers: [{ provide: TodoService, useValue: {} }],
      imports: [FormsModule],
    })

    expect(screen.getByPlaceholderText('Title')).toBeInTheDocument()
    expect(screen.getByPlaceholderText('Description')).toBeInTheDocument()
    expect(screen.getByText('Submit')).toBeInTheDocument()
    expect(screen.getByText('Submit')).toBeDisabled()
  })

  it('enables submit button when there is content in both fields', async () => {
    await render(TodoInputComponent, {
      providers: [{ provide: TodoService, useValue: {} }],
      imports: [FormsModule],
    })

    userEvent.type(screen.getByPlaceholderText('Title'), 'My title')
    userEvent.type(screen.getByPlaceholderText('Description'), 'My description')

    expect(screen.getByText('Submit')).not.toBeDisabled()
  })
})
