import { Component, OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs'
import { TodoService } from '../../services/todo.service'

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss'],
})
export class TodoInputComponent implements OnDestroy {
  title = ''
  description = ''

  submitSubs = new Subscription()

  constructor(private todoService: TodoService) {}

  ngOnDestroy() {
    this.submitSubs.unsubscribe()
  }

  submit() {
    const subscription = this.todoService
      .addTodo({ title: this.title, description: this.description })
      .subscribe()

    this.submitSubs.add(subscription)
  }

  canSubmit() {
    return this.title.length > 0 && this.description.length > 0
  }
}
