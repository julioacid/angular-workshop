import { nanoid } from 'nanoid'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { tap } from 'rxjs/operators'
import { Todo } from '../../app/models/todo'
import { TODOS } from '../fixtures'

export class TodoMemoryService {
  private todos$$ = new BehaviorSubject<Todo[]>(TODOS)

  getTodos() {
    return this.todos$$.asObservable()
  }

  addTodo({ title, description }: Pick<Todo, 'title' | 'description'>) {
    const newTodo: Todo = {
      id: nanoid(4),
      title,
      description,
      isCompleted: false,
    }

    return of(newTodo).pipe(tap(() => this.todos$$.next([newTodo, ...this.todos$$.value])))
  }

  completeTodo(id: string) {
    return new Observable<Todo>((subscriber) => {
      const todos = [...this.todos$$.value]
      const todoIndex = todos.findIndex((todo) => todo.id === id)

      if (todoIndex < 0) throw new Error(`Todo with id ${id} was not found`)
      const completedTodo = { ...todos[todoIndex], isCompleted: true }

      todos[todoIndex] = completedTodo

      subscriber.next(completedTodo)
      this.todos$$.next(todos)
      subscriber.complete()
    })
  }

  deleteTodo(id: string) {
    return new Observable<void>((subscriber) => {
      const todos = [...this.todos$$.value]
      if (!todos.some((todo) => todo.id === id)) throw new Error(`Todo with id ${id} was not found`)

      this.todos$$.next(todos.filter((todo) => todo.id !== id))
      subscriber.complete()
    })
  }
}
