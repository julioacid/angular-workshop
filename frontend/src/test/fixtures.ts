import { Todo } from '../app/models/todo'

export const COMPLETED_TODO: Todo = {
  id: 'rXjS',
  title: 'Sacar al perro a dar un paseo',
  description: 'Recuerda que mínimo ha de ser de una hora',
  isCompleted: true,
}

export const PENDING_TODO: Todo = {
  id: 'rEaC',
  title: 'Lavar la ropa',
  description: 'Hay que desmontar también la montaña de la silla',
  isCompleted: false,
}

export const TODOS: Todo[] = [COMPLETED_TODO, PENDING_TODO]
