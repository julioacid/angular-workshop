# TODO App Angular - Angular Workshop

This project consists in a backend written in Nest.js and a frontend written in Angular. It is a simple application that allows you to track, create, delete and complete todos.

In order to execute you need to open both frontend and backend folder from a terminal and run:

```
yarn
yarn start
```
