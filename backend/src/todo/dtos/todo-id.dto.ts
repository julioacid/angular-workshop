import { IsString } from 'class-validator'

export class TodoIdDTO {
  @IsString()
  todoId!: string
}
