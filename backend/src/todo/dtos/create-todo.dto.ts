import { IsString } from 'class-validator'

export class CreateTodoDTO {
  @IsString()
  title!: string

  @IsString()
  description!: string
}
