import { Todo } from '../Todo'

export class ShowTodoDTO {
  id!: string
  title!: string
  description!: string
  isCompleted!: boolean

  static fromDomain(todo: Todo): ShowTodoDTO {
    return {
      id: todo.id,
      title: todo.title,
      description: todo.description,
      isCompleted: todo.isCompleted,
    }
  }
}
