import { Injectable, NotFoundException } from '@nestjs/common'
import { nanoid } from 'nanoid'
import { Todo } from './Todo'

@Injectable()
export class TodoService {
  private todos: Todo[] = [
    {
      id: nanoid(4),
      title: 'Sacar al perro a dar un paseo',
      description: 'Recuerda que mínimo ha de ser de una hora',
      isCompleted: false,
    },
    {
      id: nanoid(4),
      title: 'Lavar la ropa',
      description: 'Hay que desmontar también la montaña de la silla',
      isCompleted: true,
    },
  ]

  getTodos() {
    return this.todos
  }

  addTodo(todo: Todo) {
    this.todos = [todo, ...this.todos]

    return todo
  }

  deleteTodo(todoId: string) {
    this.ensureTodoExists(todoId)

    this.todos = this.todos.filter((todo) => todo.id !== todoId)
  }

  markAsCompleted(todoId: string) {
    const todoIndex = this.getTodoIndex(todoId)

    const completedTodo = { ...this.todos[todoIndex], isCompleted: true }
    this.todos[todoIndex] = completedTodo

    return completedTodo
  }

  private ensureTodoExists(todoId: string) {
    this.getTodo(todoId)
  }

  private getTodo(todoId: string) {
    const todo = this.todos.find((todo) => todo.id === todoId)

    if (!todo) throw new NotFoundException(`Todo with id ${todoId} was not found`)

    return todo
  }

  private getTodoIndex(todoId: string) {
    const todoIndex = this.todos.findIndex((todo) => todo.id === todoId)

    if (todoIndex < 0) throw new NotFoundException(`Todo index with id ${todoId} was not found`)

    return todoIndex
  }
}
