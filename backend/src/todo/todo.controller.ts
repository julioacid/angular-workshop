import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common'
import { nanoid } from 'nanoid'
import { CreateTodoDTO } from './dtos/create-todo.dto'
import { TodoService } from './todo.service'
import { TodoIdDTO } from './dtos/todo-id.dto'
import { ShowTodoDTO } from './dtos/show-todo.dto'

@Controller('todos')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getTodos() {
    return this.todoService.getTodos().map(ShowTodoDTO.fromDomain)
  }

  @Post()
  addTodo(@Body() { title, description }: CreateTodoDTO) {
    const createdTodo = this.todoService.addTodo({
      id: nanoid(4),
      title,
      description,
      isCompleted: false,
    })

    return ShowTodoDTO.fromDomain(createdTodo)
  }

  @Delete(':todoId')
  deleteTodo(@Param() { todoId }: TodoIdDTO) {
    return this.todoService.deleteTodo(todoId)
  }

  @Patch(':todoId/complete')
  markAsCompleted(@Param() { todoId }: TodoIdDTO) {
    const updatedTodo = this.todoService.markAsCompleted(todoId)

    return ShowTodoDTO.fromDomain(updatedTodo)
  }
}
